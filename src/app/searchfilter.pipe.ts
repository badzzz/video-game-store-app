import { Pipe, PipeTransform } from '@angular/core';
import { Game } from './shared/game';

@Pipe({
  name: 'searchfilter'
})
export class SearchfilterPipe implements PipeTransform {

  transform(game:Game[],searchValue:string):Game[]  {
    if(!game || !searchValue)
    {
      return game;
    }
    return game.filter(
      gam=>
      gam.gameName.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
      ||
      gam.gameGenre.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
      
    )
  }

}
