import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { FooterListComponent } from './core/footer/footer-list/footer-list.component';
import { GamesModule } from './games/games.module';




@NgModule({
  declarations: [
    AppComponent,
    FooterListComponent,
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GamesModule,
    CoreModule,
    

  ],
  providers: [],
  bootstrap: [AppComponent,]
})
export class AppModule {
  
 }
