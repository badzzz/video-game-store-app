import { Directive, ElementRef, Input } from "@angular/core";

@Directive({
    selector: "[autofocus]"
})
export class AutofocusDirective
{
    private focus = true;
  

    constructor(private elementRef: ElementRef)
    {
    }

    ngOnInit()
    {
      this.elementRef.nativeElement.focus();
    }

   
}