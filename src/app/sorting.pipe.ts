import { Pipe, PipeTransform } from '@angular/core';
import { Game } from './shared/game';

@Pipe({
  name: 'sorting'
})
export class SortingPipe implements PipeTransform {


  // key = "itemCode";
  //   reverse:boolean = false ;
  //   sort(key: string){
  //     this.key=key;
  //     this.reverse=!this.reverse;
  //   }

  transform(game:Game[]):Game[]  {
    return game.sort(function(a,b){
      return a.itemCode - b.itemCode
    })
  }

}
