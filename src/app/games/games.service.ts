import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GAMES } from "../shared/mock-games";
import { Game } from "../shared/game";
import { Basket } from "../shared/basket";
import { chosenGames } from "../shared/mock-basket";

@Injectable({
  providedIn: 'root'
})
export class GamesService {
  getGames(): Observable<Game[]> {
    return of(GAMES);
  }

  getBasket(): Observable<Basket[]> {
    return of(chosenGames);
  }

  addToBasket(basketItem: Basket): Observable<number> {

   console.log(basketItem)
    return of(chosenGames.push(basketItem));

  }
  removeBasket2(BasketUser:any):Observable<Basket[]>{
    //Remove item from array
    console.log(BasketUser);
    const index: number = chosenGames.findIndex(BasketIdd => BasketIdd.itemCode === BasketUser );
    console.log(index);
    return of(chosenGames.splice(index, 1));

  //  constructor() { }
}





}
