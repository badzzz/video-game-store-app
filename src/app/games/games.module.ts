import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamesListComponent } from './games-list/games-list.component';
import { GamesService } from './games.service';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { SearchfilterPipe } from '../searchfilter.pipe';
import { BasketListComponent } from './basket-list/basket-list.component';
import { RouterModule } from '@angular/router';
import { GamePageComponent } from './game-page/game-page.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { ContactsComponent } from './contacts/contacts.component';



@NgModule({
  declarations: [GamesListComponent,SearchfilterPipe,BasketListComponent, GamePageComponent,CheckoutComponent, ContactsComponent],
  providers: [GamesService],
  imports: [CommonModule, SharedModule,FormsModule,RouterModule,YouTubePlayerModule],
  exports:[SearchfilterPipe]
})
export class GamesModule { }
