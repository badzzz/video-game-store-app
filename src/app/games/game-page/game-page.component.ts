import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Game } from 'src/app/shared/game';
import {GAMES} from '../../shared/mock-games'

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.scss']
})
export class GamePageComponent implements OnInit {

  constructor(private route: ActivatedRoute,) { }
  public game : Game | undefined;
   
  

 name:string = "";
  ngOnInit(): void {

      // First get the product id from the current route.
  const routeParams = this.route.snapshot.paramMap;
  const gameCodeFromRoute = Number(routeParams.get('gameCode'));

  // Find the product that correspond with the id provided in route.
  this.game = GAMES.find(game => game.itemCode === gameCodeFromRoute); 
  }

}
