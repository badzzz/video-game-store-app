import { Component, OnInit } from '@angular/core';
import { GamesService } from '../games.service';
import { Basket } from '../../shared/basket';

@Component({
  selector: 'app-basket-list',
  templateUrl: './basket-list.component.html',
  styleUrls: ['./basket-list.component.scss']
})
export class BasketListComponent implements OnInit {
  public baskets: Basket[] = []
  public totalPrice: number = 0

  constructor(
    private gamesService: GamesService,
  ) {}

  ngOnInit(): void {
    this.loadBasket();
    this.getTotalPrice();
  }

  // get to basket section check otu games.service.ts connection
  private loadBasket(): void {
    this.gamesService
      .getBasket()
      .subscribe((baskets: Basket[]) => this.baskets = baskets);
  }

  public removeBasket(basket:Basket): void{
    console.log(basket)
    this.gamesService.removeBasket2(basket.itemCode);
  }

  getTotalPrice(): void {
    this.baskets.forEach(item => {
      this.totalPrice += item.cost
     })
  }

}
