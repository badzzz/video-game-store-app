import { Component, OnInit } from '@angular/core';
import { GamesService } from '../games.service';
import { Basket } from '../../shared/basket';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  public baskets: Basket[] = []
  public totalPrice: number = 0


  constructor( private gamesService: GamesService,
    ) { }
  ngOnInit(): void {
    this.loadBasket();
    this.getTotalPrice();
  }

  public checkOutItem = {
    
    }

// get to basket section check otu games.service.ts connection
  private loadBasket(): void {
  this.gamesService
    .getBasket()
    .subscribe((baskets: Basket[]) => this.baskets = baskets);
  }

    getTotalPrice(): void {
      this.baskets.forEach(item => {
        this.totalPrice += item.cost
       })
    }


}
