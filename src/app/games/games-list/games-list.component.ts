
import { Component, OnInit } from '@angular/core';
import { Basket } from 'src/app/shared/basket';
import { Game } from '../../shared/game';
import { GamesService } from '../games.service';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss']
})
export class GamesListComponent implements OnInit {
  public games: Game[] = [];
  searchValue:string | any ; 

  constructor(
    private gamesService: GamesService,
  ) { }
  
  
  ngOnInit(): void {
    this.loadGames();
  }

  private loadGames(): void {
    this.gamesService
      .getGames()
      .subscribe((games: Game[]) => this.games =games);
  }

  public addToBasket(gameItem: Game) {
      alert('Added to basket');
      const tempData: any = { ...gameItem }
      delete tempData.stockQuantity
      tempData.quantity = 1
    this.gamesService.addToBasket(tempData)
  }

}
