import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamesListComponent } from './games/games-list/games-list.component'
import { BasketListComponent} from './games/basket-list/basket-list.component'
import { GamePageComponent } from './games/game-page/game-page.component';
import { CheckoutComponent } from './games/checkout/checkout.component';
import { ContactsComponent } from './games/contacts/contacts.component';


const routes: Routes = [
  { path: "games-list", component: GamesListComponent },


  { path: "basket-list", component: BasketListComponent },

  { path: "game/:gameCode",component:GamePageComponent},

  { path: "contacts",component:ContactsComponent},

  { path: "checkout", component: CheckoutComponent},

  { path: "**", redirectTo: "games-list" }
  



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
