export interface Basket {
        itemCode: number;
        image?: string;
        gameName: string;
        gameGenre: string;
        releaseDate: string;
        quantity?: number;
        cost: number;
        info?: string;
        shortDescription?: string;
        
}
