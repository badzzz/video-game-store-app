export interface Game {
    itemCode: number;
    gameName: string;
    gameGenre: string;
    releaseDate: string;
    stockQuantity: number;
    cost: number;
    image?: string;
    info?: string;
    shortDescription?: string;
    video?:string
}


// game information interface created