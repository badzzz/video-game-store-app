import { Game } from "./game";

export const GAMES: Game[] = [
    {
        itemCode: 101,
        gameName: "Half-Life",
        gameGenre: "Action Shooter",
        releaseDate: "19.11.1998",
        stockQuantity: 3,
        cost: 20.99,
        image: "https://upload.wikimedia.org/wikipedia/ru/thumb/e/e5/HLbox.jpg/274px-HLbox.jpg",
        shortDescription: "HλLF-LIFE, is a science fiction first-person shooter developed and published by Valve. The player takes the perspective of the scientist Gordon Freeman who struggles to escape an underground research facility after a failed experiment causes a massive alien invasion through a dimensional rift.",
        video:"wtIp8jOo8_o"
    },

    {
        itemCode: 102,
        gameName: "StarCraft",
        gameGenre: "Strategy RTS",
        releaseDate: "31.03.1998",
        stockQuantity: 4,
        cost: 15.99,
        image: "https://upload.wikimedia.org/wikipedia/ru/0/0f/StarCraft.front_cover.jpg",
        shortDescription: "StarCraft is a military science fiction media franchise strategy game created by Blizzard Entertainment.The series, set in the beginning of the 26th century, centers on a galactic struggle for dominance among four species—the adaptable and mobile Terrans, the ever-evolving insectoid Zerg, the powerful and enigmatic Protoss, and the godlike Xel'Naga creator race—in a distant part of the Milky Way galaxy known as the Koprulu Sector.",
        video : "MVbeoSPqRs4"
    },

    {
        itemCode: 103,
        gameName: "Diablo",
        gameGenre: "Role Playing Game / RPG",
        releaseDate: "31.12.1996",
        stockQuantity: 11,
        cost: 13.99,
        image: "https://upload.wikimedia.org/wikipedia/ru/thumb/b/b7/Diablo.jpg/274px-Diablo.jpg",
        shortDescription: "Diablo is an action role-playing hack and slash dungeon crawler video game series developed by Blizzard Entertainment. The series is set in the dark fantasy world of Sanctuary, and its characters are primarily humans, angels, and various classes of demons and monsters. ",
        video : "0SSYzl9fXOQ"
    },

    {
        itemCode: 104,
        gameName: "Grand Theft Auto IV",
        gameGenre: "Action Arcade",
        releaseDate: "29.04.2008",
        stockQuantity: 10,
        cost: 14.99,
        image: "https://support.rockstargames.com/dist/img/es_es/categories/5b7e7950cae5d8848d85f4468ca65f3a.jpg",
        shortDescription: "Grand Theft Auto IV is an action-adventure game played from a third-person perspective. Players complete missions—linear scenarios with set objectives—to progress through the story. Composed of the fictional city of Liberty City, the world is larger in area than most earlier Grand Theft Auto series entries."

    },

    {
        itemCode: 105,
        gameName: "World of Warcraft",
        gameGenre: "Role Playing Game / RPG",
        releaseDate: "23.11.2004",
        stockQuantity: 5,
        cost: 34.99,
        image: "https://s1.gaming-cdn.com/images/products/150/orig/world-of-warcraft-new-player-edition-cover.jpg",
        shortDescription: "World of Warcraft (WoW), massively multiplayer online role-playing game (MMORPG) created by the American company Blizzard Entertainment and released on November 14, 2004."

    },

    {
        itemCode: 106,
        gameName: "Warcraft III",
        gameGenre: "Strategy RTS",
        releaseDate: "03.07.2002",
        stockQuantity: 7,
        cost: 28.50,
        image: "https://s3.gaming-cdn.com/images/products/2270/orig/warcraft-3-battlechest-cover.jpg",
        shortDescription: "Warcraft III is set several years after the events of Warcraft II, and tells the story of the Burning Legion's attempt to conquer the fictional world of Azeroth with the help of an army of the Undead, led by fallen paladin Arthas Menethil."

    },


    {
        itemCode: 107,
        gameName: "Call of Duty",
        gameGenre: "Action Shooter",
        releaseDate: "29.10.2003",
        stockQuantity: 9,
        cost: 32.90,
        image: "https://upload.wikimedia.org/wikipedia/ru/thumb/4/41/Codbox.jpg/274px-Codbox.jpg",
        shortDescription: "Call of Duty, electronic game that brought new advances to the first-person shooter genre. Call of Duty allowed players to advance through World War II in a series of campaigns or to battle it out against human opponents in its multiplayer mode."
    },


    {
        itemCode: 108,
        gameName: "Unreal",
        gameGenre: "Action Shooter",
        releaseDate: "30.09.1998",
        stockQuantity: 2,
        cost: 13.99,
        image: "https://gamestracker.org/_ld/76/7602.jpg",
        shortDescription: "Unreal is a first-person shooter video game developed by Epic MegaGames and Digital Extremes and published by GT Interactive in May 1998. The player takes on the part of Prisoner 849 who crashes on alien planet NaPali and fights his way out to escape from it."
    },


    {
        itemCode: 109,
        gameName: "Street Fighter V",
        gameGenre: "Fighting",
        releaseDate: "16.02.2016",
        stockQuantity: 3,
        cost: 15.99,
        image: "https://upload.wikimedia.org/wikipedia/en/8/80/Street_Fighter_V_box_artwork.png",
        shortDescription: "Street Fighter V is a fighting game developed by Capcom. The game features sixteen characters at launch, choose one and fight for your victory!"
    },

    {
        itemCode: 110,
        gameName: "Mortal Combat X",
        gameGenre: "Fighting",
        releaseDate: "02.06.2014",
        stockQuantity: 6,
        cost: 17.99,
        image: "https://upload.wikimedia.org/wikipedia/en/d/d0/Mortal_Kombat_X_Cover_Art.png",
        shortDescription: "Mortal Kombat X is a fighting game in which two characters fight against each other using a variety of attacks, including special moves, and the series' trademark gruesome finishing moves. The game allows two players to face each other (either locally or online), or a single player to play against the CPU."
    },
    // * 101 - 110 games added 02.05.2021
    {
        itemCode : 111,
        gameName : "Battlefield 1",
        gameGenre : "Shooter",
        releaseDate:"21.10.2016",
        stockQuantity:10,
        cost:40.22,
        image:"https://s1.gaming-cdn.com/images/products/2241/orig/battlefield-1-revolution-edition-cover.jpg",
        shortDescription:"world war 1 game",
        //video:"VHDmlMVWIwQ",
        // p:"Battlefield 1 (also known as BF1) is the fifteenth installment in the Battlefield Series developed by DICE and published by EA. The game is set during World War I. It marks the first installment exclusive to eighth-generation consoles and was released worldwide on October 21, 2016.The game was released on Steam on June 11th, 2020, alongside Battlefield 3, Battlefield 4, Battlefield Hardline and Battlefield V.",
        // backulr:"../../../assets/image/in-the-name-of-the-tsar-rev-2.jpg",
        // requirements1:"Processor (AMD): AMD FX-6350.",
        // requirements2:"Processor (Intel): Core i5 6600K.",
        // requirements3:"Graphics card (AMD): AMD Radeon HD 7850 2GB.",
        // requirements4:"Graphics card (Nvidia): Nvidia GeForce GTX 660 2GB",
        // requirements5:"DirectX: 11.0 Compatible video card or equivalent.",
        // logo:"BF1",
        // link:"https://www.ea.com/ru-ru/games/battlefield/battlefield-1",
    },
    {
        itemCode:112,
        gameName: "WOW: ShadowLands",
        gameGenre:"RPG",
        releaseDate:"21.11.2020",
        stockQuantity:20,
        cost:50.99,
        image:"https://s1.gaming-cdn.com/images/products/5757/orig/world-of-warcraft-shadowlands-cover.jpg",
        shortDescription:"best game",
        //video:"s4gBChg6AII",
        // p:"World of Warcraft: Shadowlands (shortened shadow, shado, or sl) is the eighth expansion for World of Warcraft. Set primarily in the eponymous region of the Shadowlands, the story is dealing with the return of Sylvanas Windrunner breaking the barrier between mortal world of Azeroth and the Shadowlands - the realm of death. The expansion was officially announced on November 1, 2019, at BlizzCon 2019.",
        // backulr:"../../../assets/image/shadow.jpg",
        // requirements1:"CPU: Intel Core i5-3450 or AMD FX 8300",
        // requirements2:"RAM: 4GB RAM (8GB if using supported integrated graphics)",
        // requirements3:"VIDEO CARD: NVIDIA GeForce GTX 760 or AMD Radeon RX 560 or Intel UHD 630 (45W)",
        // requirements4:"FREE DISK SPACE: 100 GB",
        // requirements5:"PIXEL SHADER: 5.0,VERTEX SHADER: 5.0",
        // logo:"ShadowLands",
        // link:"https://worldofwarcraft.com/en-us/shadowlands",
        
    },
    {
        itemCode:113,
        gameName : "Call of Duty: Cold War",
        gameGenre:"Action Shooter",
        releaseDate:"08.19.2020",
        stockQuantity:10,
        cost:40.99,
        image:"https://i.pinimg.com/originals/41/dd/72/41dd725903829daf77fa497805068ccf.jpg",
        shortDescription:"Another new series of call of duty",
        //video:"aTS9n_m7TW0",
        // p:"Call of Duty: Black Ops Cold War is a first-person shooter video game developed by Treyarch and Raven Software for PlayStation 4, PlayStation 5, Xbox One, Xbox Series X/S and Microsoft Windows. It is set in the early 1980s and is a direct sequel to the original Call of Duty: Black Ops. Call of Duty: Black Ops Cold War is the seventeenth game in the Call of Duty franchise and the sixth game in the Black Ops series. Announced on August 26th, 2020, the game was released on November 13th, 2020.",
        // backulr:"../../../assets/image/cold war.jpg",
        // requirements1:"Windows 7 64 Bit (SP1) or Windows 10 64 Bit (v.1803 or higher)",
        // requirements2:"Intel Core i5-2500K or AMD Ryzen R5 1600X processor",
        // requirements3:"12GB RAM",
        // requirements4:"82GB HD space",
        // requirements5:"NVIDIA GeForce GTX 970 / GTX 1660 Super or Radeon R9 390 / AMD RX 580",
        // logo:"Cold",
        // link:"https://www.callofduty.com/ru/blackopscoldwar",
    },
    {
        itemCode:114,
        gameName:"OverWatch",
        gameGenre:"Action Shooter",
        releaseDate:"05.03.2016",
        stockQuantity:30,
        cost:30.99,
        image:"https://image.api.playstation.com/vulcan/ap/rnd/202010/0714/VBANminggz6xesnf7PokdsT4.png",
        shortDescription:"Overwatch multiplayer game of shooter",
        //video:"FqnKB22pOC0",
        // p:"Overwatch is a multiplayer team-based first-person shooter developed and published by Blizzard Entertainment.In a time of global crisis, an international task force of heroes banded together to restore peace to a war-torn world. This organization, known as Overwatch, ended the crisis and helped maintain peace for a generation, inspiring an era of exploration, innovation, and discovery.After many years, Overwatch's influence waned and it was eventually disbanded. Now in the wake of its dismantling, conflict is rising once again. Overwatch may be gone... but the world still needs heroes.",
        // backulr:"../../../assets/image/over.png",
        // requirements1:"NVIDIA® GeForce® GTX 460, ATI Radeon™ HD 4850, or Intel® HD Graphics 4400",
        // requirements2:"	4 GB RAM",
        // requirements3:"30 GB available hard drive space",
        // requirements4:"1024 x 768 minimum display resolution",
        // requirements5:"Broadband internet connection",
        // logo:"Over",
        // link:"https://playoverwatch.com/ru-ru/",
    },
    {
        itemCode:115,
        gameName:"GTA V",
        gameGenre:"Action Arcade",
        releaseDate:"09.17.2013",
        stockQuantity:40,
        cost:20.99,
        image:"https://upload.wikimedia.org/wikipedia/ru/c/c8/GTAV_Official_Cover_Art.jpg",
        shortDescription:"Another Gta series",
        //video:"QkkoHAzjnUs",
        // p:"Grand Theft Auto V (also known as Grand Theft Auto Five, GTA 5, GTA V, or GTAV[2]) is a video game developed by Rockstar North. It is the fifteenth installment in the Grand Theft Auto series and the successor of Grand Theft Auto IV.",
        // backulr:"../../../assets/image/GTA V.jpg",
        // requirements1:"",
        // requirements2:"",
        // requirements3:"",
        // requirements4:"",
        // requirements5:"",
    },
    {
        itemCode:116,
        gameName:"UFC 3",
        gameGenre:"Fighting",
        releaseDate:"02.02.2018",
        stockQuantity:20,
        cost:39.99,
        image:"https://images.hind.ee/5947/30/ea-sports-ufc-3-ps4-2.jpg",
        shortDescription:"Fighting UFC for playstation",
        //video:"Pgh7dGxVKWw",
        // p:"EA Sports UFC 3 is a mixed martial arts fighting video game developed by EA Canada and was published by EA Sports .A sequel, EA Sports UFC 4, was released on August 14, 2020.",
        // backulr:"../../../assets/image/UFC.jpg",
        // requirements1:"PS4",
        // requirements2:"XBOX360",
        // requirements3:""
        
        
    },
    {
        itemCode:117,
        gameName:"Resident Evil: Village",
        gameGenre:"Survival horror",
        releaseDate:"18.04.2021",
        stockQuantity:15,
        cost:49.99,
        image:"https://image.api.playstation.com/vulcan/ap/rnd/202101/0812/FkzwjnJknkrFlozkTdeQBMub.png",
        //video:"CNpIfP4vtrE",
        // p:"Resident Evil Village, stylised as RESIDENT EVIL VII.I.AGE and known in Japan as BIOHAZARD VILLAGE  is a first person Survival Horror title developed by Capcom. It was released on 7 May 2021. It is the first Resident Evil game for the ninth console generation.",
        // backulr:"../../../assets/image/Resident.jpg",
        // requirements1:"",
        // requirements2:"",
        // requirements3:"",
        // requirements4:"",
        // requirements5:"",

    },
    {
        itemCode:118,
        gameName:"Dying Light 2",
        gameGenre:"Action Shooter",
        releaseDate:"10.06.2018",
        stockQuantity:30,
        cost:27.99,
        image:"https://upload.wikimedia.org/wikipedia/ru/1/10/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dying_Light_2.jpg",
        //video:"dkWS0xGutSY",
        // p:"Dying Light 2 is an upcoming open world first-person survival horror video game developed by Techland and published by Techland Publishing, it is a sequel to Dying Light.",
        // backulr:"../../../assets/image/light.jpg",
        // requirements1:"",
        // requirements2:"",
        // requirements3:"",
        // requirements4:"",
        // requirements5:"",
    }

];


